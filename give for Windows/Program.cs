﻿using System;
using System . Collections . Generic;
using System . Linq;
using System . Text;
using System . Threading . Tasks;

namespace give_for_Windows {
	class Program {
		static void Main( string[] args ) {
			if (args.Length == 0 ) {
				Console . WriteLine ( "Unknown command."
				+"\r\nGIVE <--[HERE");
			} else {
				var ITEMS = System . IO . File . ReadAllLines ( System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "ITEMS.CSV" ));
				foreach ( var X in ITEMS ) {
					if (X.Split(',')[1] == args[0] ) {
						Console . WriteLine ("Given [1] " + X . Split ( ',' )[1] + " to " + Environment.UserName);
						Environment . Exit ( 0x00 );
					}
				}
				Console . WriteLine ( "Unknown command."
					+ "\r\nGIVE " + args[0] + " <--[HERE]" );
			}
		}
	}
}
